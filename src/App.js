import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css'
import {
  BrowserRouter,
  Switch,
  Route,
  //Redirect
} from 'react-router-dom'
import StartupPage from './components/StartupPage';
import TranslationPage from './components/TranslationPage';
import ProfilePage from './components/ProfilePage';

function App() {
  return (
    <BrowserRouter>
      <div className="App">
        <Switch>
          <Route path="/" exact component={StartupPage} />
          <Route path="/translate" exact component={TranslationPage} />
          <Route path="/profile" exact component={ProfilePage} />
        </Switch>
      </div>
    </BrowserRouter>
  );
}

export default App;
