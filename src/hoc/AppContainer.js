const AppContainer = ({ children }) => {
    return (
        <div className="container mt-5">{children}</div>
    )
}

export default AppContainer