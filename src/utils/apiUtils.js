export const handleResponse = async response => {
    if (!response.ok) {
        const { error = "something unexpected went wrong" } = await response.json()
        throw new Error(error)
    }
    return await response.json()
}