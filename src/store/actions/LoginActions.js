export const ACTION_LOGIN_ATTEMPTING = '[login] ATTEMPT'
export const ACTION_LOGIN_SUCCESS = '[login] SUCCESS'
export const ACTION_LOGIN_ERROR = '[login] ERROR'

// I am on purpose not exporting an individual function for each of these action types,
// because I think the readability in the program is much more clear withouth them

// When I dispatch an action, I will pass in an object as parameter myself, like for example this:
// dispatch( {type: ACTION_LOGIN_ATTEMPTING, payload: whatever} )
