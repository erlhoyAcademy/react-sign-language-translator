import { ACTION_TRANSLATION_POST } from "../actions/TranslationActions"
import TranslationsAPI from "../../API/translationsAPI"

const TranslationMiddleware = ({ dispatch }) => next => action => {
    next(action)

    if (action.type === ACTION_TRANSLATION_POST) {
        const userText = action.payload
        TranslationsAPI.postTranslation(userText)
    }
}
export default TranslationMiddleware