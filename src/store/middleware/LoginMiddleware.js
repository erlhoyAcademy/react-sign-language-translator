import { LoginAPI } from '../../API/loginAPI'
import { ACTION_LOGIN_ATTEMPTING, ACTION_LOGIN_SUCCESS, ACTION_LOGIN_ERROR } from '../actions/LoginActions'

const LoginMiddleware = ({ dispatch }) => next => action => {
    next(action)

    if (action.type === ACTION_LOGIN_ATTEMPTING) {
        LoginAPI.login(action.payload)
            .then(
                dispatch( {type: ACTION_LOGIN_SUCCESS, payload: action.payload} ))
            .catch(error => {
                dispatch( {type: ACTION_LOGIN_ERROR, payload: error} )
            })
    }

    if (action.type === ACTION_LOGIN_SUCCESS) {
        localStorage.setItem('session', action.payload)
    }
}
export default LoginMiddleware