import { applyMiddleware } from "redux";
import LoginMiddleware from "./LoginMiddleware";
import TranslationMiddleware from "./TranslationMiddleware";

export default applyMiddleware(
    LoginMiddleware,
    TranslationMiddleware
)