import { ACTION_LOGIN_ATTEMPTING, ACTION_LOGIN_ERROR, ACTION_LOGIN_SUCCESS } from '../actions/LoginActions'

const initialState = {
    sessionIsSet: false,
    error: ""
}

const LoginReducer = (state = initialState, action) => {

    switch(action.type) {

        case ACTION_LOGIN_ATTEMPTING:
            return { sessionIsSet: false, error: "" }

        case ACTION_LOGIN_SUCCESS:
            return { sessionIsSet: true, error: "" }

        case ACTION_LOGIN_ERROR:
            const errorMessage = action.payload
            return { sessionIsSet: false, error: errorMessage}

        default: return state
    }
}
export default LoginReducer