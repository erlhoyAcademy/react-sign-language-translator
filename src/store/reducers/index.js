import { combineReducers } from 'redux'
import LoginReducer from './LoginReducer'

const appReducer = combineReducers({
    LoginReducer
})

export default appReducer