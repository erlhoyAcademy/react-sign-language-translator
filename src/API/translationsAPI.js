import { handleResponse } from '../utils/apiUtils'

const TranslationsAPI = {

    // POST request to insert translation text into json server db (running on localhost:8000)
    postTranslation(translationText) {
        return fetch("http://localhost:8000/translations", {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({translation: translationText})
        }).then(handleResponse)
    },

    // GET request to get the translation texts from json server db (running on localhost:8000)
    getTranslations() {
        return fetch('http://localhost:8000/translations', {
            method: "GET",
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(handleResponse)
    },

    deleteTranslations(arrayOfIds) {
        arrayOfIds.forEach(id => {
            return fetch(`http://localhost:8000/translations/${id}`, {method: "DELETE"})
        })
    }
}

export default TranslationsAPI