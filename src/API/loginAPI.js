import { handleResponse } from "../utils/apiUtils"

export const LoginAPI = {
    login(username) {
        // POST request to insert new username to json server db (running on localhost:5000)
        return fetch("http://localhost:5000/usernames", {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify( {"username": username} )
        }).then(handleResponse)
    }
}