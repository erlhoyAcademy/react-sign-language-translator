
const ImageComponent = props => {
    return(
        <>
            {
                props.inputTextArray.map((char, index) => {

                    const specialCharacters = [".", "-", "_", "!", "@", "'", "*", "æ", "ø", "å", "?"]
                    if (char === " ") {
                        return <span key={index}>(whitespace)</span>
                    }
                    else if (specialCharacters.includes(char.toLowerCase())) {
                        return <span key={index}>(special char)</span>
                    }
                    else {
                        return <img key={index} alt="Sign language character" src={`LostInTranslation_Resources/individial_signs/${char}.png`}></img>
                    }
                })
            }
        </>
    )
}

export default ImageComponent