import { useEffect, useState } from "react";
import AppContainer from "../hoc/AppContainer";
import TranslationsAPI from "../API/translationsAPI"
import { Redirect } from "react-router";

const ProfilePage = () => {

    const [arrayOfTranslationIndex, setArrayOfTranslationIndex] = useState([])
    const [arrayOfTranslationText, setArrayOfTranslationText] = useState([])
    const [logOutClicked, setLogOutClicked] = useState(false)

    useEffect(() => {
        TranslationsAPI.getTranslations()
            .then(
                (response => setArrayOfTranslationIndex(response.map(x => x.id))) 
                && 
                (response => setArrayOfTranslationText(response.map(x => x.translation))))
            .then(console.log(arrayOfTranslationIndex))
    }, [])

    const onClearClicked = () => {
        //TranslationsAPI.deleteTranslations( arrayOfTranslationIndex.slice(-10).reverse() )
    }

    const onLogoutClicked = () => {
        localStorage.removeItem("session")
        setLogOutClicked(true)
        TranslationsAPI.deleteTranslations( arrayOfTranslationIndex )
    }

    return(
        <>
            { (localStorage.getItem("session") === null || logOutClicked) && <Redirect to="/"/> }
            { localStorage.getItem("session") !== null &&
                <AppContainer>
                    <section>
                        <h1>Your last translations</h1>
                        { arrayOfTranslationText.length > 0 && arrayOfTranslationText.slice(-10).reverse().map((text, index) => <p key={index}>{text}</p>) }
                    </section>
                    <button onClick={onClearClicked}>Clear</button>
                    <button onClick={onLogoutClicked}>Log out</button>
                </AppContainer>
            }
        </>
    )
}
export default ProfilePage