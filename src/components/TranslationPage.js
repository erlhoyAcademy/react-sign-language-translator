import AppContainer from "../hoc/AppContainer"
import { Redirect } from "react-router-dom"
import { useDispatch } from 'react-redux'
import { useState } from 'react'
import { ACTION_TRANSLATION_POST} from "../store/actions/TranslationActions"
import ImageComponent from '../components/ImageComponent'

const TranslationPage = () => {

    // Makes it possible to dispatch redux actions
    const dispatch = useDispatch()

    // React hooks - keeps track of the input state and stores it locally in the component
    const [inputText, setInputText] = useState("")
    const [goToProfile, setGoToPrile] = useState(false)

    const [userIsTyping, setUserIsTyping] = useState(false)
    const [userClickedTranslate, setUserClickedTranslate] = useState(false)

    // Triggers when the user is typing characters from keyboard
    const onUserInputChange = (event) => {
        setUserClickedTranslate(false)
        setUserIsTyping(true)
        setInputText(event.target.value)
    }

    // Triggers when clicking the "Translate" button
    const onTranslateClicked = event => {
        event.preventDefault()
        setUserIsTyping(false)
        setUserClickedTranslate(true)
        dispatch( {type: ACTION_TRANSLATION_POST, payload: inputText} )
    }

    // Triggers when clicking on Go to profile button
    const handleGoToProfile = () => {
        setGoToPrile(true)
    }

    return(
    <>
        { localStorage.getItem("session") === null && <Redirect to="/" /> }
        { localStorage.getItem("session") !== null &&
        <AppContainer>
            <main>
                <form onSubmit={ onTranslateClicked }>
                    <input onChange={ onUserInputChange } id="txtInputField" type="text" placeholder="Write a word or a short sentence?"></input>
                    <input className="btn btn-primary" type="submit" value="Translate" />
                </form>
                <div>
                    { userIsTyping && <p>Waiting for you to click Translate...</p> }
                    { userClickedTranslate && <ImageComponent inputTextArray={ inputText.split('') } /> }
                </div>
                <button onClick={ handleGoToProfile } className="btn btn-large">Go To Profile</button>
                { goToProfile && <Redirect to="/profile" /> }
            </main>
        </AppContainer> }
    </>
    )
}

export default TranslationPage