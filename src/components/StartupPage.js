import AppContainer from '../hoc/AppContainer'
import { useDispatch, useSelector } from 'react-redux'
import { ACTION_LOGIN_ATTEMPTING } from '../store/actions/LoginActions'
import { Redirect } from 'react-router-dom'
import { useEffect, useState } from 'react'

const StartupPage = () => {
    
    const { sessionIsSet } = useSelector(state => state.LoginReducer)

    const [loggedIn, setLoggedIn] = useState(false)
    const [userInput, setUserInput] = useState("")

    useEffect(() => {
        setLoggedIn (sessionIsSet) // sessionIsSet is a boolean value. This is needed for initialization
        if (localStorage.getItem("session") !== null) { // Log in, and redirect if there is already an active session
            setLoggedIn(true)
        }
    }, [sessionIsSet])


    const dispatch = useDispatch()

    // Triggers when the user clicks on the "Log in" button
    const handleLoginClick = event => {
        event.preventDefault()
        // This action triggers a small chain of actions and middleware operations, and will set the session if successfull
        dispatch( {type: ACTION_LOGIN_ATTEMPTING, payload: userInput} )
    }

    const onUserInputChange = event => {
        setUserInput(event.target.value)
    }

    return (

        <>
            { loggedIn && <Redirect to="/translate"/> }
            { ! loggedIn &&
                <AppContainer>
                    <section className="startPageDescription">
                        <img alt="hello imgae" src="LostInTranslation_Resources/Logo-Hello.png"/>
                        <div>
                            <h1>Lost in Translation</h1>
                            <p>Get started</p>
                        </div>
                    </section>
                    <form onSubmit={handleLoginClick}>
                        <label className="form-label">
                            Username:
                            <input onChange={onUserInputChange} className="form-control" type="text" name="loginInput" placeholder="<Your username>" />
                        </label>
                        <input type="submit" className="btn btn-primary" value="Log in"/>
                    </form>
                </AppContainer>
            }
        </>

    )
}

export default StartupPage